import React from "react";

const Suscripcion=(props) =>{
    return (
        <div>

            <div className="backgroundForm">
                <div className="textTitleForm"><span>{props.title}</span></div>

                <div>
                    <input className="inputForm" type="email" name="" id="" placeholder="Ingrese su correo"/>

                    <input className="buttonForm" type="button" value="Suscribirme"/>
                </div>
            </div>


        </div>
    )
}

export default Suscripcion
