import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

//BOOTSTRAP
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

//ESTILOS
import './styles/style.css';
import Logo from "./componets/Logo";
import BotonHeader from "./componets/BotonHeader";
import ImagenHeader from "./componets/ImagenHeader";
import Menu from "./componets/Menu";
import Slider from "./componets/Slider";
import Catalogo from "./componets/Catalogo";
import Calendario from "./componets/Calendario";
import Suscripcion from "./componets/Suscripcion";
import BotonFooter from "./componets/BotonFooter";

const App = () => {

    const menu = {
        name: 'Mil Detalles',
        items: [
            {
                title: 'OCACIÓN',
            },
            {
                title: 'ARREGLOS',
            },
            {
                title: 'RAMOS',
            },
            {
                title: 'CONDOLECENCIAS',
            },
            {
                title: 'NACIMIENTOS',
            },
            {
                title: 'REGALOS',
            },

        ]
    }

    const slider = {
        imgs: [
            {
                src: 'img/diadelamujer.png',
                title: 'Feliz dia  de la mujer',
            },
            {
                src: 'img/outlet.png',
                title: 'Aprovecha nuestras ventas outlet',
            },
            {
                src: 'img/app.png',
                title: 'Descarga nuestra nueva App',
            }

        ]
    }

    const catalogo = {
        products: [
            {
                title: 'ROSAS',
                price: 'S/ 116.00',
                src: 'img/tulipan.png'

            },
            {
                title: 'TULIPANES',
                price: 'S/ 165.00',
                src: 'img/rosas.png'

            },
            {
                title: 'ASTROMELIAS',
                price: 'S/ 116.00',
                src: 'img/astromelia.jpg'

            },
            {
                title: 'GIRASALOES',
                price: 'S/ 135.00',
                src: 'img/girasoles.jpg'

            },
        ]
    }


    const footer = {
        items: [
            {
                title:'Quienes somos'
            },
            {
                title:'Cómo comprar'
            },
            {
                title:'Formas de pago'
            },
            {
                title:'Preguntas frecuentes'
            },
            {
                title:'Delivery'
            },
            {
                title:'Contacto'
            }
        ]
    }


    return (
        <div>
            {/* Encabezado*/}
            <div class="contenedor">
                {/* Fila de encabezado */}
                <div class="fila headerTop">
                    <div class="col-lg-7 col-md-7 col-sm-0 col-xs-0">
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        <BotonHeader title="Registrarse"/>
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        <BotonHeader title="Iniciar Sesión"/>
                    </div>

                    {/* redes sociales */}
                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        <ImagenHeader href="https://www.facebook.com/" className="imgTop" src="img/facebook.svg"/>
                        <ImagenHeader href="https://www.instagram.com/" className="imgTop" src="img/instagram.svg"/>
                    </div>

                    {/* carrito de compras */}
                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        <div className="backgroundShopping">
                            <ImagenHeader className="imgTop" src="img/shopping.svg"/>
                        </div>
                    </div>
                </div>

                {/* Fila de logo y contacto */}
                <div class="fila">
                    <div class="col-lg-2 col-md-1 col-sm-0 col-xs-0">
                    </div>

                    {/* logo de la  empresa */}
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        <ImagenHeader className="imgMenu" src="img/menu.svg"/>
                        <ImagenHeader className="imgLogo" src="img/logo.png"/>
                    </div>

                    <div class="col-lg-1 col-md-2 col-sm-7 col-xs-7">
                    </div>

                    {/* call center */}
                    <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0">
                        <ImagenHeader className="imgContact" src="img/phone.svg"/>
                        <span class="textContact">054 - 463256</span>
                    </div>
                    {/* número de whatsapp */}
                    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1">
                        <ImagenHeader className="imgContact" src="img/whatsapp.svg"/>
                        <span class="textContact">958020402</span>
                    </div>
                    {/* busqueda de productos */}
                    <div class="col-lg-3 col-md-3 col-sm-1 col-xs-1">
                        <div>
                            <input class="inputSearch col-sm-0 col-xs-0" type="search"
                                   placeholder="Buscar Productos..."/>
                            <ImagenHeader className="imgSearch" src="img/search.svg"/>
                        </div>
                    </div>
                    {/* carrito de compras para pantallas pequeñas */}
                    <div class="col-lg-0 col-md-0 col-sm-1 col-xs-1">
                        <a href="#">
                            <div class="col-lg-0 col-md-0 backgroundShopping">
                                <ImagenHeader className="imgTop" src="img/shopping.svg"/>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            {/* barra de menus */}
            <Menu name={menu.name} items={menu.items}/>

            {/* contenido principal  */}
            <div className="contenedor">
                {/* fila de nuestro slider */}
                <div className="fila">
                    <div className="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                    </div>
                    {/* slider */}
                    <div className="col-lg-10 col-md-10 col-sm-0 col-xs-0">
                        <Slider imgs={slider.imgs}/>
                    </div>

                    <div className="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                    </div>
                </div>
            </div>


            {/* seccion de productos ofertados */}
            <div className="contenedor">
                {/* fila de titulo  */}
                <div className="fila">
                    <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    </div>
                    <div className="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <div className="backgroundTitle">
                            <span className="textTitle">Productos Destacados</span>
                        </div>
                    </div>
                    <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    </div>
                </div>
                {/* fila de productos ofertados */}
                <div className="fila">
                    <div className="col-lg-2 col-md-2 col-sm-0 col-xs-0">
                    </div>
                    <Catalogo products={catalogo.products}/>
                </div>
            </div>

            {/* seccion informativa  */}
            <div class="contenedor">
                {/* fila para fechas para recordar y suscripción  */}
                <div class="fila">
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-11 col-xs-11">
                        {/* fechas para recordar  */}
                        <Calendario title={"FECHAS PARA RECORDAR"}
                                    subtitle={"Te ayudamos a recordar esos dias especiales"}/>
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        {/* suscripcion para recibir ofertas  */}
                        <Suscripcion title={"RECIBIR OFERTAS"}/>
                    </div>

                    <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                    </div>
                </div>
            </div>

            {/* seccion call center pantallas pequeñas  */}

            <div class="contenedor">
                {/* fila de titulo  */}
                <div class="fila">
                    <div class="col-lg-0 col-md-0 col-sm-12 col-xs-12">
                        <span class="textTitle">Call Center</span>
                    </div>
                </div>
                {/* fila de numeros de contacto  */}
                <div class="fila">
                    <div class="col-lg-0 col-md-0 col-sm-2 col-xs-2">
                    </div>
                    {/* telefono fijo */}
                    <div class="col-lg-0 col-md-0 col-sm-4 col-xs-4">
                        <ImagenHeader className="imgContact" src="img/phone.svg"/>
                        <span className="textContactCallCenter">054 - 463256</span>
                    </div>
                    {/* número de whatsapp */}
                    <div class="col-lg-0 col-md-0 col-sm-4 col-xs-4">
                        <ImagenHeader className="imgContact" src="img/whatsapp.svg"/>
                        <span className="textContactCallCenter">958020402</span>
                    </div>
                </div>
                {/* fila de redes sociales  */}
                <div class="fila">
                    <div class="col-lg-0 col-md-0 col-sm-5 col-xs-5">
                    </div>

                    <div class="col-lg-0 col-md-0 col-sm-2 col-xs-2">
                        <ImagenHeader href="https://www.facebook.com/" className="imgTop" src="img/facebook.svg"/>
                        <ImagenHeader href="https://www.instagram.com/" className="imgTop" src="img/instagram.svg"/>
                    </div>
                </div>
            </div>


            {/* pies de pagina  */}
            <footer>
                <div class="contenedor">
                    {/* fila de enlaces de mapa del sitio  */}
                    <div class="fila headerTop">
                        <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        </div>

                        {/* enlaces de uso frecuente  */}
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">

                            <BotonFooter items={footer.items} />

                        </div>

                        <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                        </div>

                    </div>

                    {/* fila de derechos reservados  */}
                    <div class="fila headerTop">
                        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                        </div>

                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4">
                            <span class="textDateRememberHelp">&copy; Derechos Reservados 2021</span>
                        </div>

                        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-4">
                        </div>
                    </div>
                </div>
            </footer>


        </div>
    )
}

ReactDOM.render(
    <App/>
    , document.getElementById('root'))
