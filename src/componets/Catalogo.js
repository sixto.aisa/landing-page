import Producto from "./Producto";

const Catalogo=(props) =>{
    return (
        <div>
            {
                props.products.map((producto, index) => {
                    return (
                        <Producto src={producto.src} title={producto.title} price={producto.price} />
                    );
                })
            }
        </div>
    )
}

export default Catalogo
