import ImagenHeader from "./ImagenHeader";
import React from "react";

const Producto=(props) =>{
    return (
        <div>

            <div className="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                <div>
                    {/* imagen del producto */}
                    <ImagenHeader className="imgFlower" src={props.src} />
                    <div className="textFlower">{props.title}</div>
                    {/* precio del producto  */}
                    <div className="textPriceFlower">{props.price}</div>
                </div>
                <footer>
                    {/* agregar producto al carrito de compras */}
                    <a href="#">
                        <div className="backgroundAddShoppingCart backgroundShopping">
                            <div className="textAddShoppingCart">
                                <span>Agregar al Carrito</span>
                                <ImagenHeader className="imgTop" src="img/shopping.svg" />
                            </div>
                        </div>
                    </a>
                </footer>
            </div>


        </div>
    )
}

export default Producto
