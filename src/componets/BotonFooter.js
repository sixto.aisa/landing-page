import React from "react";
import Producto from "./Producto";

const BotonFooter=(props) =>{
    return (
        <div>
                <ul>
                    {
                        props.items.map((item, index) => {
                            return (
                                <li><a className="footerItem" href="#">{item.title}</a></li>
                            );
                        })
                    }
                </ul>


        </div>
    )
}

export default BotonFooter



