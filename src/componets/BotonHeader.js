const BotonHeader=(props) =>{
    return (
        <div>
            <a href="#">
                <div className="backgroundHeader">
                    <span className="textHeader">{props.title}</span>
                </div>
            </a>
        </div>
    )
}

export default BotonHeader
