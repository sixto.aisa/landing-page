const Logo=(props) =>{
    return (
        <div>

            <nav>
                <div className="contenedor">
                    {/* fila de menu principal  */}
                    <div className="fila">
                        {/* icono de menu pantallas pequeñas  */}
                        <div className="col-lg-0 col-md-0 col-sm-1 col-xs-1">
                            <span className="textLogo">Menú</span>
                        </div>


                        <div className="col-lg-2 col-md-1 col-sm-0 col-xs-0">
                        </div>
                        {/* frace de la empresa */}
                        <div className="col-lg-1 col-md-1 col-sm-0 col-xs-0">
                            <span className="textLogo">{props.name}</span>
                        </div>
                        {/* opciones del menú principal */}
                        <div className="col-lg-8 col-md-9 col-sm-10 col-xs-10">
                            <ul>
                                {
                                    props.items.map((menu, index) => {
                                        return (
                                            <li><a className="navItem" href="#">{menu.title}</a></li>
                                        );
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>


        </div>
    )
}

export default Logo
