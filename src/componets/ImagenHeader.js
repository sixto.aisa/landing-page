const ImagenHeader=(props) =>{
    return (
        <div className="d-inline">
            <a href={props.href} target="_blank">
                <img className={props.className} src={props.src}/>
            </a>
        </div>
    )
}

export default ImagenHeader
