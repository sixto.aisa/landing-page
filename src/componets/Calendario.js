import React from "react";
import ImagenHeader from "./ImagenHeader";

const Calendario=(props) =>{
    return (
        <div>

            <div className="backgroundDateRemember">
                <div className="fila">
                    <div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <ImagenHeader className="imgCalendar" src="img/calendar.png" />
                    </div>

                    <div className="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <span className="textDateRemember">{props.title}</span>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <span className="textDateRememberHelp">{props.subtitle}</span>
                        <input className="buttonRemember" type="button" value="Recordar fecha"/>
                    </div>
                </div>
            </div>


        </div>
    )
}

export default Calendario
