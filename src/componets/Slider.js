const Slider=(props) =>{
    return (
        <div>

            <section className="contenedorSlider">
                <ul id="slider">

                        {
                            props.imgs.map((img, index) => {
                                return (
                                    <li><img src={img.src} alt={img.title} /></li>
                                );
                            })
                        }
                </ul>
            </section>


        </div>
    )
}

export default Slider
